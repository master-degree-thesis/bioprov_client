% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/services.R
\name{auth}
\alias{auth}
\title{Authentication Service}
\usage{
auth(username, password)
}
\arguments{
\item{username}{A string value with a username}

\item{password}{A string value with a password of the username}
}
\value{
The access token to call the protectec services
}
\description{
Authenticate the user and get a access token.
}
