% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/services.R
\name{getEntityByURI}
\alias{getEntityByURI}
\title{Query an Entity by URI}
\usage{
getEntityByURI(uri)
}
\arguments{
\item{uri}{A URI (Uniform Resource Identifier)}
}
\value{
An Entity record
}
\description{
Query an specific Entity record by ID (URI).
}
