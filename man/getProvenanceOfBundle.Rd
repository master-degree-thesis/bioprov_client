% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/services.R
\name{getProvenanceOfBundle}
\alias{getProvenanceOfBundle}
\title{Query Provenance of a Context}
\usage{
getProvenanceOfBundle(uri = NULL, format = "json")
}
\arguments{
\item{uri}{An URI (Uniform Resource Identifier)}
}
\value{
A collection of Provenance records
}
\description{
Query provenance records of a Context
}
